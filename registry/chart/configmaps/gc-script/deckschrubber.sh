#!/usr/bin/env ash
set -euxo pipefail

echo "waiting for registry "
timeout 120 sh -c "while ! timeout 1 curl --silent -o /dev/null --fail registry.registry.svc/v2/; do echo -n .; sleep 1; done";
echo -n "done"

exec deckschrubber \
  -registry=http://registry.registry.svc \
  -repos=1000 \
  -ntag='^(v?(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(\.(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*)?(\+[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*)?$|latest|build-cache|master|main|cache)' \
  -day=7 \
  -latest=10
