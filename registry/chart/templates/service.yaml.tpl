apiVersion: v1
kind: Service
metadata:
  name: registry
  namespace: registry
  labels:
    app: registry
spec:
  clusterIP: "{{ .Values.service_ip }}"
  ports:
    - name: registry
      port: 5000
      targetPort: registry
    - name: debug
      port: 5001
      targetPort: debug
    - name: registry-http
      port: 80
      targetPort: registry
  selector:
    name: registry
