apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/proxy-body-size: 1G
    ingress-waf/additional-crs: |
      SecAction "id:900200,phase:1,nolog,pass,t:none,setvar:\'tx.allowed_methods=GET HEAD POST PUT OPTIONS DELETE PATCH\'"
      SecRuleRemoveById 920340 920420
  name: registry
  namespace: registry
spec:
  rules:
    - host: "{{ .Values.domain }}"
      http:
        paths:
          - backend:
              service:
                name: registry
                port:
                  number: 5000
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.domain }}"
      secretName: "{{ .Values.tls_wildcard_secret }}"
