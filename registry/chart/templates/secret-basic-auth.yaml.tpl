apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: registry
type: Opaque
stringData:
  auth: "{{ .Values.basic_auth }}"
